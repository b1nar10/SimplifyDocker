#!/bin/bash
 
echo "  ____  _                 _ _  __         ____             _               "
echo " / ___|(_)_ __ ___  _ __ | (_)/ _|_   _  |  _ \  ___   ___| | _____ _ __   "
echo " \___ \| | '_ \` _ \| '_ \| | | |_| | | | | | | |/ _ \ / __| |/ / _ \ '__|  "
echo "  ___) | | | | | | | |_) | | |  _| |_| | | |_| | (_) | (__|   <  __/ |     "
echo " |____/|_|_| |_| |_| .__/|_|_|_|  \__, | |____/ \___/ \___|_|\_\___|_|     "
echo "                   |_|            |___/                                    "


PS3='Please enter your choice: '
opcions=("New Project" "List Projects" "Modify Project" "Language" "Exit")
select opt in "${opcions[@]}"
do
    case $opt in
        "New Project")
            echo "Create a new project"
            ;;
        "List Projects")
            echo "List Projects"
            ;;
        "Modify Project")
            echo "you chose choice $REPLY which is $opt"
            ;;
        "Language")
            echo "Change Language"
            ;;
        "Exit")
            break
            ;;
        *) echo "Invalid option $REPLY";;
    esac
done