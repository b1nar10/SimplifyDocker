<!DOCTYPE html>
<html lang="gl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilos.css">
    <script src="versions.js"></script>    
    <title>SimplifyDocker</title>
</head>
<body>
        <h1>Simplify Docker</h1>

        <form method="POST" action="scripts.php">

            
            <label>Escolle unha Linguaxe</label><br><br>
            
            
            <select name="linguaxe" id="linguaxe" onchange="ver_version()">
                <option value="c">C</option>
                <option value="cpp">C++</option>
                <option value="cs">C#</option>
                <option value="clj">Clojure</option>
                <option value="dart">Dart</option>
                <option value="jsd">Deno</option>
                <option value="ex">Elixir</option>
                <option value="erl">Erlang</option>
                <option value="hs">Haskell</option>
                <option value="java">Java</option>
                <option value="js">JavaScript</option>
                <option value="kt">Kotlin</option>
                <option value="jsn">NodeJS</option>
                <option value="h">Objetive-C</option>
                <option value="php">PHP</option>
                <option value="py">Python</option>
                <option value="ruby">Ruby</option>
                <option value="rs">Rust</option>
                <option value="scala">Scala</option>
                <option value="ts">TypeScript</option>

            </select><br><br>


            <label>Escolle unha versión: </label><br><br>
            <select name="version" id="version" required>
                <option value="0"></option>
            </select><br><br>


            <label>Introduce a Ruta do Proxecto :</label><br>
            <input type="Text" name="ruta" required><br><br> 

           
    
            <input type="submit" name="enviar" value="Enviar">
            
        </form>

</body>
</html>